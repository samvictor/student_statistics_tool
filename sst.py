"""
	This is the file can be used to access the full functionality of SST. It uses
	student_data.py to get data, then the interface is decided by using the options
	-s for scripting mode or -w to start the web server. If neither options are given,
	by default, the user is presented with a layered interface where data is provided at
	every level and the user can choose which data he/she wants to see.
"""
from student_data import *


parser = argparse.ArgumentParser() 
parser.add_argument("-u", "--username", help="Instructor Username") 
parser.add_argument("-p", "--password", help="Instructor Password") 

parser.add_argument("-s", "--scriptmode", help="Allow full functionality using arguments", action="store_true") 
parser.add_argument("-w", "--webserver", help="Start the flask web server", action="store_true") 

temptext = "Store data in a file called data.txt in current directory: "
temptext += os.path.dirname(os.path.realpath(__file__)) + "/"
parser.add_argument("-d", "--datafile", help=temptext, action="store_true") 

parser.add_argument("-r", "--reports", help="Print reports. Use only with --scriptmode.", action="store_true") 
temptext = "Retrieve source codes and store them in the current directory: "
temptext += os.path.dirname(os.path.realpath(__file__)) + "/. Use only with --scriptmode."
parser.add_argument("-c", "--sourcecodes", help=temptext, action="store_true") 
parser.add_argument("-cr", "--createresults",
		help="Create Results if they are not already in the database. Use only with --scriptmode.", action="store_true") 
parser.add_argument("-e", "--exercise", help="Get data for a specific exercise. Use only with --scriptmode.") 
parser.add_argument("-ae", "--allexercises", help="Get data for all exercises. Use only with --scriptmode.", action="store_true") 
parser.add_argument("--user", help="Get data for a specific user. Use only with --scriptmode.") 
parser.add_argument("-au", "--allusers", help="Get data for all specific users. Use only with --scriptmode.", action="store_true") 

args = parser.parse_args() 

# Handling possible option errors
if args.scriptmode and args.webserver: 
	sys.exit("Error: Incompatible options --scriptmode and --webserver") 

if (args.reports or args.sourcecodes or args.createresults or args.exercise or args.allexercises or
		args.user or args.allusers) and not args.scriptmode: 
	temp_text = "Error: Only --username, --password, --datafile, and --webserver" 
	temp_text += " options are allowed without --scriptmode enabled" 
	sys.exit(temp_text) 

if (not args.username or not args.password) and not args.webserver:
		sys.exit("Error: No username or password entered. Use --username and --password")

if args.scriptmode:
	import sst_cli
	sst_cli.cli(args)
	sys.exit(0)

if args.webserver:
	import web_interface
	if web_interface.HOST:
		web_interface.app.run(host=web_interface.HOST)
	else:
		web_interface.app.run()
	sys.exit(0)
	
if args.datafile:
	data_file = open("data.txt", 'w+')


username = args.username
password = args.password

Populate_exercise_list(username, password)

print "The following exercises were found:"
for e in exercises:
	print '\t', e.name
print ""
print "The following users were found:"

Populate_data(exercises)
for u in users:
	print "\t", u.name
print ""

	
# Non script mode
if (solutions_wo_results):
	print "No results or reports were found for the following solutions"
	for s in solutions_wo_results:
		print "\t", "Submission to ", s.exercise.name, " submitted by ", s.user.name

	if raw_input("Generate results? (\"Y\" or \"N\"): ").lower() == "y": 
		for s in solutions_wo_results:
			print "Generating result for submission to ", s.exercise.name, " submitted by ", s.user.name
			Generate_results(s)


u_or_ex = raw_input("Enter either \"users\" or \"exercises\": ")
u_or_ex = u_or_ex.lower().lstrip()

if u_or_ex == "":
	u_or_ex = "error"
elif u_or_ex[0] == "e":
	u_or_ex = "exercises"
elif u_or_ex[0] == "u":
	u_or_ex = "users"
else :
	u_or_ex = "error"

while (u_or_ex == "error") :
	print "The following exercises were found:"
	for e in exercises:
		print '\t', e.name
	print ""

	print "The following users were found:"
	for u in users:
		print "\t", u.name
	print ""
	
	u_or_ex = raw_input("Enter either \"users\" or \"exercises\": ")
	u_or_ex = u_or_ex.lower().lstrip().rstrip()
	
	if u_or_ex == "":
		u_or_ex = "error"
	elif u_or_ex[0] == "e":
		u_or_ex = "exercises"
	elif u_or_ex[0] == "u":
		u_or_ex = "users"
	else :
		u_or_ex = "error"

if u_or_ex == "exercises" :
	print "Choose an exercise or enter \"*\" for all exercises:\n"
	for e in exercises:
		print '\t', e.name
	exercise_name = raw_input("\n")
	exercise_name = exercise_name.lower().lstrip().rstrip()
	
	if exercise_name == "*":
		exercise_name = ""
		print "\nDisplaying data for all exercises"
		
		for exercise in exercises:
			print exercise.name, ":"
			
			latest_solutions = exercise.solutions
			num_attempted = len(latest_solutions)
			print num_attempted, " users attempted this exercise."
			
			num_full_score = 0
			num_zero = 0
			num_between = 0
			for s in latest_solutions:
				if s.get_latest_result():
					if s.get_latest_result()['report']:
						temp_report = s.get_latest_result()['report']
					else:
						print "no report found for " + s.user.name + " for this exercise"
						continue
				else:
					print "no result found for " + s.user.name + " for this exercise"
					continue
				
				if temp_report['stats']['score'] >= temp_report['stats']['full_score']:
					num_full_score += 1
				elif temp_report['stats']['score'] == 0:
					num_zero += 1
				else:
					num_between += 1
			
			print "\tFull score: ", num_full_score, " users"
			print "\tBetween zero and full score: ", num_between, " users"
			print "\tZero: ", num_zero, " users\n"

	else :
		exercise = None
		for e in exercises:
			if e.name.lower().lstrip().rstrip() == exercise_name:
				exercise = e
		if not exercise:
			print "Error getting exercise"
		print "\nDisplaying data for ", exercise.name, ":"
		
		latest_solutions = exercise.solutions
		num_attempted = len(latest_solutions)
		print num_attempted, " users attempted this exercise."
		
		num_full_score = 0
		num_zero = 0
		num_between = 0
		for s in latest_solutions:
			if s.get_latest_result():
				temp_report = s.get_latest_result()['report']
			else:
				print "no results found for " + s.user.name + " for this exercise"
				continue
			
			temp_report = s.get_latest_result()['report']
			if temp_report['stats']['score'] >= temp_report['stats']['full_score']:
				num_full_score += 1
			elif temp_report['stats']['score'] == 0:
				num_zero += 1
			else:
				num_between += 1
		
		print "\tFull score: ", num_full_score, " users"
		print "\tBetween zero and full score: ", num_between, " users"
		print "\tZero: ", num_zero, " users\n"
		
		
	
elif u_or_ex == "users" :
	print "Choose a user or enter \"*\" for all users:\n"
	for u in users:
		print "\t", u.name
	user_name = raw_input("\n")
	user_name = user_name.lower().lstrip().rstrip()
	
	if user_name == "*":
		user_name = ""
		print "\nDisplaying data for all users"
		
		for user in users:
			print user.name, ":"
			
			latest_solutions = user.solutions 

			"""
			print "contents of latest_solutions:"
			for l in latest_solutions:
				print "\t", "name of exercise = ", l.exercise.name
				print "\t", "results:"
				for r in l.results:
					print r
			"""

			num_attempted = len(latest_solutions)
			print "This user attempted ", num_attempted, " exercises."
			
			for s in latest_solutions:
				if not s.get_latest_result():
					print "no result found for " + s.exercise.name + " exercise for this student"
					continue
				if not s.get_latest_result()['report']:
					print "no report found for " + s.exercise.name + " exercise for this student"
					continue
	
				temp_score = s.get_latest_result()['report']['stats']['score']
				temp_full = s.get_latest_result()['report']['stats']['full_score']
				print "\t", s.exercise.name, " Score: ", temp_score, "/", temp_full
		
			print ""
		
	else :
		for u in users:
			if u.name.lower().lstrip().rstrip() == user_name:
				user = u
		print "\nDisplaying data for ", user.name, ":"
		
		latest_solutions = user.solutions
		num_attempted = len(latest_solutions)
		print "This user attempted ", num_attempted, " exercises."
		
		for s in latest_solutions:
			if not s.get_latest_result():
				print "no result found for " + s.exercise.name + " exercise for this student"
				continue 
			temp_score = s.get_latest_result()['report']['stats']['score']
			temp_full = s.get_latest_result()['report']['stats']['full_score']
			print "\t", s.exercise.name, " Score: ", temp_score, "/", temp_full
		
		print ""
		
rep_or_source = ""
while (rep_or_source.lower().lstrip().rstrip() != "q"):
	temp_text = "Enter \"reports\" for full reports,"
	temp_text += " \"sourcecodes\" to retrieve sourcecodes, or \"q\" to quit: "
	rep_or_source = raw_input(temp_text)
	
	if not rep_or_source:
		continue
	rep_or_source = rep_or_source.lower().lstrip().rstrip()[0]
	
	if rep_or_source == "c" or rep_or_source == "s":
		rep_or_source = "sourcecodes"
		if u_or_ex == "exercises":
			# get all source codes for all exercises
			if not exercise_name:
				print "Retrieving source codes for all exercises"
				for e in exercises:
					Get_sourcecodes_from_exercise(e)
			# get all source codes for "exercise_name"
			else:
				if exercise:
					print "Retrieving source codes for ", exercise.name
					Get_sourcecodes_from_exercise(exercise)
				else:
					print "Error finding chosen exercise"
		elif u_or_ex == "users":
			# get all source codes for all users
			if not user_name:
				print "Retrieving source codes for all users"
				for e in exercises:
					Get_sourcecodes_from_exercise(e)
			# get all source codes for "user_name"
			else:
				if user:
					print "Retrieving source codes for ", user.name
					for s in user.solutions:
						Get_sourcecodes_from_solution(s)
				else:
					print "Error finding chosen user"
		print "Source codes retrieved"
	
	elif rep_or_source == "r":
		rep_or_source = "reports"
		if u_or_ex == "exercises":
			# get all reports for all exercises
			if not exercise_name:
				print "Displaying reports for all exercises\n"
				for e in exercises:
					print e.name, ":"
					for s in e.solutions:
						print Result_to_String(s.get_latest_result(), e.name, s.user.name)
						print ""
					print "\n" 
			# get all reports for "exercise_name"
			else:
				if exercise:
					print "Displaying reports for ", exercise.name
					for s in exercise.solutions:
						print Result_to_String(s.get_latest_result(), exercise.name, s.user.name)
						print ""
				else:
					print "Error finding chosen exercise"
		elif u_or_ex == "users":
			# get all reports for all users
			if not user_name:
				print "Displaying reports for all users\n"
				for u in users:
					print u.name, ":"
					for s in u.solutions:
						print Result_to_String(s.get_latest_result(), s.exercise.name, s.user.name)
						print ""
					print "\n"
				
			# get all source codes for "user_name"
			else:
				if user:
					print "Displaying reports for ",user.name, ":"
					for s in user.solutions:
						print Result_to_String(s.get_latest_result(), s.exercise.name, s.user.name)
						print ""
				else:
					print "Error finding chosen user"

if args.datafile:
	data_file.close()



	
