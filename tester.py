import requests
import sys
import time

# python Testcli.py <username>
username = sys.argv[1]
option = sys.argv[2] # "nr" makes program exit before creating results
password = 'password'
masteru = 'gadmin'
masterp = 'gadmin123'
repo_url = 'ssh://hg@bitbucket.org/teamglass7311/teacher-repo'
repo_url2 = 'ssh://hg@bitbucket.org/samvictor/teacher-repo2'
BASE_URL = 'https://192.168.33.10/aurum/api'
ACCOUNT_API_BASE = 'account'
EXERCISE_API_BASE = 'exercise'

# create users
data = {
    'username': username +  "_teacher",
    'password': password,
    'masteru': masteru,
    'masterp': masterp
       }
account_api = BASE_URL + '/' + ACCOUNT_API_BASE
r = requests.post(account_api, data=data, verify=False)
if(r.status_code == 200 or r.status_code == 201):
    print (r.json()['message'])
else:
    print 'Account not created\nEither account already exists or the server is unreachable'


data = {
    'username': username + "_Sam",
    'password': password,
    'masteru': masteru,
    'masterp': masterp
       }
account_api = BASE_URL + '/' + ACCOUNT_API_BASE
r = requests.post(account_api, data=data, verify=False)
if(r.status_code == 200 or r.status_code == 201):
    print (r.json()['message'])
else:
    print 'Account not created\nEither account already exists or the server is unreachable'


data = {
    'username': username + "_Fitz",
    'password': password,
    'masteru': masteru,
    'masterp': masterp
       }
account_api = BASE_URL + '/' + ACCOUNT_API_BASE
r = requests.post(account_api, data=data, verify=False)
if(r.status_code == 200 or r.status_code == 201):
    print (r.json()['message'])
else:
    print 'Account not created\nEither account already exists or the server is unreachable'


data = {
    'username': username + "_Ian",
    'password': password,
    'masteru': masteru,
    'masterp': masterp
       }
account_api = BASE_URL + '/' + ACCOUNT_API_BASE
r = requests.post(account_api, data=data, verify=False)
if(r.status_code == 200 or r.status_code == 201):
    print (r.json()['message'])
else:
    print 'Account not created\nEither account already exists or the server is unreachable'




# create exercises
password = password
data = { 'name': username + "_Say Hello",
         'repo_url': repo_url  }
exercise_api = BASE_URL + '/' + EXERCISE_API_BASE
r = requests.post(exercise_api, data=data, verify=False, auth=(username + "_teacher",password))
if r.status_code == 201:
    hello_token=r.json()['data']['access_token']
    hello_exid= r.json()['data']['exercise_id']
    print 'exercise created'
else:
    raise SystemExit("Failed to create exercise\nError code: " + str(r.status_code))

password = password
data = { 'name': username + "_Say Goodbye",
         'repo_url': repo_url2  }
exercise_api = BASE_URL + '/' + EXERCISE_API_BASE
r = requests.post(exercise_api, data=data, verify=False, auth=(username + "_teacher",password))
if r.status_code == 201:
    bye_token=r.json()['data']['access_token']
    bye_exid= r.json()['data']['exercise_id']
    print 'exercise created'
else:
    raise SystemExit("Failed to create exercise\nError code: " + str(r.status_code))
"""        
password = password
data = { 'name': username + "_Say Hello",
         'repo_url': repo_url  }
exercise_api = BASE_URL + '/' + EXERCISE_API_BASE
r = requests.post(exercise_api, data=data, verify=False, auth=(username + "_teacher",password))
if r.status_code == 201:
    hello_token=r.json()['data']['access_token']
    hello_exid= r.json()['data']['exercise_id']
    print 'exercise created'
else:
    raise SystemExit("Failed to create exercise\nError code: " + str(r.status_code))
"""

# submit solutions
solutions_api= BASE_URL + '/exercise'+'/'+hello_exid+'/solutions'
data = { "student_id":username + "_Ian", "src_code":"Hello Universe bye world"}
r = requests.post( solutions_api, data=data, verify=False,headers={'Authorization':hello_token})
response = r.status_code
if r.status_code ==201:
    ian_solutionid1 = r.json()['data']['solution_id']
    print (r.json()['message'])
else :
    raise SystemExit("Failed to submit solution\nError code: " + str(r.status_code))
    
# update
solutions_api= BASE_URL + '/solutions'+'/'+ian_solutionid1
data = { "student_id":username + "_Ian", "src_code":"Hello World bye world"}
r = requests.post( solutions_api, data=data, verify=False,headers={'Authorization':hello_token})
response = r.status_code
if r.status_code ==200:
    ian_solutionid2 = r.json()['data']['solution_id']
    print (r.json()['message'])
else :
    raise SystemExit("Failed to submit solution\nError code: " + str(r.status_code))

#"""
solutions_api= BASE_URL + '/exercise'+'/'+hello_exid+'/solutions'
data = { "student_id":username + "_Fitz", "src_code":"Hello World"}
r = requests.post( solutions_api, data=data, verify=False,headers={'Authorization':hello_token})
response = r.status_code
if r.status_code ==201:
    fitz_solutionid1 = r.json()['data']['solution_id']
    print (r.json()['message'])
else :
    raise SystemExit("Failed to submit solution\nError code: " + str(r.status_code))
#"""

solutions_api= BASE_URL + '/exercise'+'/'+bye_exid+'/solutions'
data = { "student_id":username + "_Fitz", "src_code":"Hello Earth bye world"}
r = requests.post( solutions_api, data=data, verify=False,headers={'Authorization':bye_token})
response = r.status_code
if r.status_code ==201:
    fitz_solutionid2 = r.json()['data']['solution_id']
    print (r.json()['message'])
else :
    raise SystemExit("Failed to submit solution\nError code: " + str(r.status_code))
"""    
solutions_api= BASE_URL + '/exercise'+'/'+hello_exid+'/solutions'
data = { "student_id":username + "_Fitz", "src_code":"Hello World"}
r = requests.post( solutions_api, data=data, verify=False,headers={'Authorization':hello_token})
response = r.status_code
if r.status_code ==201:
    fitz_solutionid1 = r.json()['data']['solution_id']
    print (r.json()['message'])
else :
    raise SystemExit("Failed to submit solution\nError code: " + str(r.status_code))
"""

solutions_api= BASE_URL + '/exercise'+'/'+hello_exid+'/solutions'
data = { "student_id":username + "_Sam", "src_code":"Hello Earth"}
r = requests.post( solutions_api, data=data, verify=False,headers={'Authorization':hello_token})
response = r.status_code
if r.status_code ==201:
    sam_solutionid1 = r.json()['data']['solution_id']
    print (r.json()['message'])
else :
    raise SystemExit("Failed to submit solution\nError code: " + str(r.status_code))

solutions_api= BASE_URL + '/solutions'+'/'+sam_solutionid1
data = { "student_id":username + "_Sam", "src_code":"Hello World bye world"}
r = requests.post( solutions_api, data=data, verify=False,headers={'Authorization':hello_token})
response = r.status_code
if r.status_code ==200:
    sam_solutionid2 = r.json()['data']['solution_id']
    print (r.json()['message'])
else :
    raise SystemExit("Failed to submit solution\nError code: " + str(r.status_code))
    
solutions_api= BASE_URL + '/exercise'+'/'+bye_exid+'/solutions'
data = { "student_id":username + "_Sam", "src_code":"Hello World bye Earth"}
r = requests.post( solutions_api, data=data, verify=False,headers={'Authorization':bye_token})
response = r.status_code
if r.status_code ==201:
    sam_solutionid3 = r.json()['data']['solution_id']
    print (r.json()['message'])
else :
    raise SystemExit("Failed to submit solution\nError code: " + str(r.status_code))

solutions_api= BASE_URL + '/solutions'+'/'+sam_solutionid3
data = { "student_id":username + "_Sam", "src_code":"Hello Earth bye world"}
r = requests.post( solutions_api, data=data, verify=False,headers={'Authorization':bye_token})
response = r.status_code
if r.status_code ==200:
    sam_solutionid4 = r.json()['data']['solution_id']
    print (r.json()['message'])
else :
    raise SystemExit("Failed to submit solution\nError code: " + str(r.status_code))

if option.lower() == "nr":
	# don't create results
	sys.exit(0)

#"""
# hello ian 1
# create results
postcreateresult= BASE_URL+'/solutions/'+ian_solutionid1+'/results'
data = {"response":response, "shared_key":hello_token}
r=requests.post(postcreateresult, data=data, verify=False,headers={'Authorization':hello_token})
if r.status_code ==201:
    ian_resultid1 = r.json()['data']['result_id']
    print (r.json()['message'])
else:
    raise SystemExit("Failed to create result\nError code: " + str(r.status_code))


# check if results are ready
while True:
    pollforjobstatus= BASE_URL+'/results/'+ian_resultid1+'/progress'
    r= requests.get(pollforjobstatus, verify=False,headers={'Authorization':hello_token})
    time.sleep(6)
    if r.status_code == 200:
        if (r.json()['data']['status']=='Complete' or r.json()['data']['status']=='Error'):
            ac_result_response = r.json()['data']['status']
            print (r.json()['message'])
            break
        else:
            print (r.json()['message'])
    else:
      raise SystemExit("Failed to request progress\nError code: " + str(r.status_code))
     
     
# get results
get_report = BASE_URL+'/results/'+ian_resultid1+'/report'
data = {"shared_key":hello_token}
r=requests.get(get_report, data=data, verify=False, headers={'Authorization':hello_token})
if r.status_code == 200:
    print r.json()['data']['stats']
    print r.json()['data']['tests']
    print r.json()['data']['artifacts']
else:
	raise SystemExit("Failed to request report\nError code: " + str(r.status_code))



"""
# hello ian 2
# create results
postcreateresult= BASE_URL+'/solutions/'+ian_solutionid2+'/results'
data = {"response":response, "shared_key":hello_token}
r=requests.post(postcreateresult, data=data, verify=False,headers={'Authorization':hello_token})
if r.status_code ==201:
    ian_resultid2 = r.json()['data']['result_id']
    print (r.json()['message'])
else:
    raise SystemExit("Failed to create result\nError code: " + str(r.status_code))


# check if results are ready
while True:
    pollforjobstatus= BASE_URL+'/results/'+ian_resultid2+'/progress'
    r= requests.get(pollforjobstatus, verify=False,headers={'Authorization':hello_token})
    time.sleep(6)
    if r.status_code == 200:
        if (r.json()['data']['status']=='Complete' or r.json()['data']['status']=='Error'):
            ac_result_response = r.json()['data']['status']
            print (r.json()['message'])
            break
        else:
            print (r.json()['message'])
    else:
      raise SystemExit("Failed to request progress\nError code: " + str(r.status_code))
     
     
# get results
get_report = BASE_URL+'/results/'+ian_resultid2+'/report'
data = {"shared_key":hello_token}
r=requests.get(get_report, data=data, verify=False, headers={'Authorization':hello_token})
if r.status_code == 200:
    print r.json()['data']['stats']
    print r.json()['data']['tests']
    print r.json()['data']['artifacts']
else:
	raise SystemExit("Failed to request report\nError code: " + str(r.status_code))
#"""


#"""
# hello fitz 1
# create results
postcreateresult= BASE_URL+'/solutions/'+fitz_solutionid1+'/results'
data = {"response":response, "shared_key":hello_token}
r=requests.post(postcreateresult, data=data, verify=False,headers={'Authorization':hello_token})
if r.status_code ==201:
    fitz_resultid1 = r.json()['data']['result_id']
    print (r.json()['message'])
else:
    raise SystemExit("Failed to create result\nError code: " + str(r.status_code))


# check if results are ready
while True:
    pollforjobstatus= BASE_URL+'/results/'+fitz_resultid1+'/progress'
    r= requests.get(pollforjobstatus, verify=False,headers={'Authorization':hello_token})
    time.sleep(6)
    if r.status_code == 200:
        if (r.json()['data']['status']=='Complete' or r.json()['data']['status']=='Error'):
            ac_result_response = r.json()['data']['status']
            print (r.json()['message'])
            break
        else:
            print (r.json()['message'])
    else:
      raise SystemExit("Failed to request progress\nError code: " + str(r.status_code))
     
     
# get results
get_report = BASE_URL+'/results/'+fitz_resultid1+'/report'
data = {"shared_key":hello_token}
r=requests.get(get_report, data=data, verify=False, headers={'Authorization':hello_token})
if r.status_code == 200:
    print r.json()['data']['stats']
    print r.json()['data']['tests']
    print r.json()['data']['artifacts']
else:
	raise SystemExit("Failed to request report\nError code: " + str(r.status_code))
#"""


#"""
# bye fitz 2
# create results
postcreateresult= BASE_URL+'/solutions/'+fitz_solutionid2+'/results'
data = {"response":response, "shared_key":bye_token}
r=requests.post(postcreateresult, data=data, verify=False,headers={'Authorization':bye_token})
if r.status_code ==201:
    fitz_resultid2 = r.json()['data']['result_id']
    print (r.json()['message'])
else:
    raise SystemExit("Failed to create result\nError code: " + str(r.status_code))


# check if results are ready
while True:
    pollforjobstatus= BASE_URL+'/results/'+fitz_resultid2+'/progress'
    r= requests.get(pollforjobstatus, verify=False,headers={'Authorization':bye_token})
    time.sleep(6)
    if r.status_code == 200:
        if (r.json()['data']['status']=='Complete' or r.json()['data']['status']=='Error'):
            ac_result_response = r.json()['data']['status']
            print (r.json()['message'])
            break
        else:
            print (r.json()['message'])
    else:
      raise SystemExit("Failed to request progress\nError code: " + str(r.status_code))
     
     
# get results
get_report = BASE_URL+'/results/'+fitz_resultid2+'/report'
data = {"shared_key":bye_token}
r=requests.get(get_report, data=data, verify=False, headers={'Authorization':bye_token})
if r.status_code == 200:
    print r.json()['data']['stats']
    print r.json()['data']['tests']
    print r.json()['data']['artifacts']
else:
	raise SystemExit("Failed to request report\nError code: " + str(r.status_code))
"""


"""
# hello fitz 1
# create results
postcreateresult= BASE_URL+'/solutions/'+fitz_solutionid1+'/results'
data = {"response":response, "shared_key":hello_token}
r=requests.post(postcreateresult, data=data, verify=False,headers={'Authorization':hello_token})
if r.status_code ==201:
    fitz_resultid1 = r.json()['data']['result_id']
    print (r.json()['message'])
else:
    raise SystemExit("Failed to create result\nError code: " + str(r.status_code))


# check if results are ready
while True:
    pollforjobstatus= BASE_URL+'/results/'+fitz_resultid1+'/progress'
    r= requests.get(pollforjobstatus, verify=False,headers={'Authorization':hello_token})
    time.sleep(6)
    if r.status_code == 200:
        if (r.json()['data']['status']=='Complete' or r.json()['data']['status']=='Error'):
            ac_result_response = r.json()['data']['status']
            print (r.json()['message'])
            break
        else:
            print (r.json()['message'])
    else:
      raise SystemExit("Failed to request progress\nError code: " + str(r.status_code))
     
     
# get results
get_report = BASE_URL+'/results/'+fitz_resultid1+'/report'
data = {"shared_key":hello_token}
r=requests.get(get_report, data=data, verify=False, headers={'Authorization':hello_token})
if r.status_code == 200:
    print r.json()['data']['stats']
    print r.json()['data']['tests']
    print r.json()['data']['artifacts']
else:
	raise SystemExit("Failed to request report\nError code: " + str(r.status_code))

#"""


#"""
# hello sam 2
# create results
postcreateresult= BASE_URL+'/solutions/'+sam_solutionid2+'/results'
data = {"response":response, "shared_key":hello_token}
r=requests.post(postcreateresult, data=data, verify=False,headers={'Authorization':hello_token})
if r.status_code ==201:
    sam_resultid2 = r.json()['data']['result_id']
    print (r.json()['message'])
else:
    raise SystemExit("Failed to create result\nError code: " + str(r.status_code))


# check if results are ready
while True:
    pollforjobstatus= BASE_URL+'/results/'+sam_resultid2+'/progress'
    r= requests.get(pollforjobstatus, verify=False,headers={'Authorization':hello_token})
    time.sleep(6)
    if r.status_code == 200:
        if (r.json()['data']['status']=='Complete' or r.json()['data']['status']=='Error'):
            ac_result_response = r.json()['data']['status']
            print (r.json()['message'])
            break
        else:
            print (r.json()['message'])
    else:
      raise SystemExit("Failed to request progress\nError code: " + str(r.status_code))
     
     
# get results
get_report = BASE_URL+'/results/'+sam_resultid2+'/report'
data = {"shared_key":hello_token}
r=requests.get(get_report, data=data, verify=False, headers={'Authorization':hello_token})
if r.status_code == 200:
    print r.json()['data']['stats']
    print r.json()['data']['tests']
    print r.json()['data']['artifacts']
else:
	raise SystemExit("Failed to request report\nError code: " + str(r.status_code))



# bye sam 4
# create results
postcreateresult= BASE_URL+'/solutions/'+sam_solutionid4+'/results'
data = {"response":response, "shared_key":bye_token}
r=requests.post(postcreateresult, data=data, verify=False,headers={'Authorization':bye_token})
if r.status_code ==201:
    sam_resultid4 = r.json()['data']['result_id']
    print (r.json()['message'])
else:
    raise SystemExit("Failed to create result\nError code: " + str(r.status_code))


# check if results are ready
while True:
    pollforjobstatus= BASE_URL+'/results/'+sam_resultid4+'/progress'
    r= requests.get(pollforjobstatus, verify=False,headers={'Authorization':bye_token})
    time.sleep(6)
    if r.status_code == 200:
        if (r.json()['data']['status']=='Complete' or r.json()['data']['status']=='Error'):
            ac_result_response = r.json()['data']['status']
            print (r.json()['message'])
            break
        else:
            print (r.json()['message'])
    else:
      raise SystemExit("Failed to request progress\nError code: " + str(r.status_code))
     
     
# get results
get_report = BASE_URL+'/results/'+sam_resultid4+'/report'
data = {"shared_key":bye_token}
r=requests.get(get_report, data=data, verify=False, headers={'Authorization':bye_token})
if r.status_code == 200:
    print r.json()['data']['stats']
    print r.json()['data']['tests']
    print r.json()['data']['artifacts']
else:
	raise SystemExit("Failed to request report\nError code: " + str(r.status_code))
"""



