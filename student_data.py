"""
	Get statistics and source code about Solutions associated with
	an instructor and Exercise name. This is the main file that gets
	data from the Aurum server, but cannot be run by itself.
"""
#TODO: replace "System exists" with "print and return"
#TODO: write to file what is written to command line
#TODO: add functionality for getting source code
#TODO: test line 118
#TODO: change input texts: only enter first letter
#TODO: check if latest result is first or last in solution.results
import requests
import sys
import time
from models import *
import argparse 
import os
#requests.packages.urllib3.disable_warnings()

#BASE_URL = 'https://192.168.33.10/aurum/api'
BASE_URL = 'https://134.74.146.30/aurum/api'


users = []
exercises = []
solutions = []
solutions_wo_results = []


# -------------------------- Functions ---------------------------

def Result_to_String(result, exercise_name, user_name):
	if not result:
		print "No result provided to Result_to_String()"
		return
	
	return_string =  """Submitted to """ + exercise_name + """ by """ + user_name

	return_string += """: 
    report: 
   	tests: 
     	    """
	for test_name in result["report"]["tests"].keys():
		return_string += test_name + """: 
        	score: """ + str(result["report"]["tests"][test_name]["score"]) + """
                description: """+ result["report"]["tests"][test_name]["description"] + """
               	full_score: """ + str(result["report"]["tests"][test_name]["full_score"]) + """
                time: """ + result["report"]["tests"][test_name]["time"] + """
	    """
	return_string += """
        stats: 
            errors: """ + str(result["report"]["stats"]["errors"]) + """
            score: """ + str(result["report"]["stats"]["score"]) + """ 
            skipped: """ + str(result["report"]["stats"]["skipped"]) + """
            passed: """ + str(result["report"]["stats"]["passed"]) + """
            failures: """ + str(result["report"]["stats"]["failures"]) + """
            total: """ + str(result["report"]["stats"]["total"]) + """
            full_score: """ + str(result["report"]["stats"]["full_score"]) + """
      	"""

	return return_string

def Populate_data(exercise_list):
	"""
		When given an exercise, return a list of results associated with that exercise.
		Populates users, solutions, solutions_wo_results, and results for each solution
	"""
	global users
	
	# get usernames and solution ids from exercise ids
	for exercise in exercise_list:
		exercise_id = exercise.id
		ac_token = exercise.ac_token
		get_solutions = BASE_URL + '/exercise/' + exercise_id + '/solutions'
		data = {"shared_key" : ac_token}
		r=requests.get(get_solutions, data=data, verify=False, headers={
																'Authorization':ac_token})
		
		if r.status_code == 200: # TODO test this
			temp_users = r.json()['data']['solution_users']
			for k in temp_users.keys() :
				user_found = False
				for u in users:
					if u.name == temp_users[k]: # user already exists
						user_found = True
						
						u.add_exercise(exercise) # give users their exercises
						
						temp_solution = Solution() # give users their solutions
						temp_solution.id = k
						temp_solution.user = u
						temp_solution.exercise = exercise
						
						if u.add_solution(temp_solution):
							solutions.append(temp_solution)
							exercise.solutions.append(temp_solution)
						else :
							for s in u.solutions:
								if s.id == k:
									s.exercise = exercise
									s.user = u
						
						exercise.add_user(u) # give exercises their users
						
				
				if not user_found: # user does not exist
					temp_user = User(temp_users[k])
					temp_solution = Solution()
					temp_solution.id = k
					temp_solution.user = temp_user
					temp_solution.exercise = exercise
					temp_user.solutions.append(temp_solution)
					temp_user.exercises.append(exercise)
					
					solutions.append(temp_solution)
					users.append(temp_user)
					
					exercise.solutions.append(temp_solution)
					exercise.users.append(temp_user)
					
		else:
			print "Error getting user names. Error code: ", r.status_code
			return
	
	for s in solutions:
		Get_results_from_solution(s)

def Get_results_from_solution(solution):
	"""
		When given a solution, return a list of results associated with that solution,
		and update that solution's "results" list.
	"""
		
	# get result ids using solution ids
	result_id_list = []
	ac_token = solution.exercise.ac_token
	get_results = BASE_URL + '/solutions/' + solution.id + '/results'
	data = {"shared_key" : ac_token}
	r=requests.get(get_results, data=data, verify=False, headers={
														'Authorization':ac_token})
	
	if r.status_code == 200:
		# create a list of result ids rather than a list of lists of result ids
		result_id_list = r.json()['data']['result_ids']
	else:
		raise SystemExit("Error getting Result IDs")
	if not result_id_list:
		print "No Results found"
		solutions_wo_results.append(solution)

	# get results using result ids
	result_list = []
	for res_id in result_id_list:
		retrieve_result = BASE_URL + '/results/' + res_id
		data = {"shared_key" : ac_token}
		r=requests.get(retrieve_result, data=data, verify=False, headers={
															'Authorization':ac_token})	
		if r.status_code == 200:
			temp_result = r.json()['data']
			if not temp_result['report'] == "None": 
				result_list.append(temp_result)
			else:
				print "No Report found for Result"
				solutions_wo_results.append(solution)
		else:
			raise SystemExit("Error getting Results")
	
	solution.results = result_list
	return result_list

def Generate_results(solution):
	"""
		First generate results on the Aurum server, then call Get_results_from_solution
		to fetch those results.
	"""
	postcreateresult= BASE_URL+'/solutions/'+solution.id+'/results'
	data = {"response":200, "shared_key":solution.exercise.ac_token} #TODO is response necessary?
	r=requests.post(postcreateresult, data=data, verify=False,
									headers={'Authorization':solution.exercise.ac_token})
	if r.status_code ==201:
		result_id = r.json()['data']['result_id']
		print (r.json()['message'])
	else:
		raise SystemExit("Failed to create result\nError code: " + str(r.status_code))
	
	while True:
		pollforjobstatus= BASE_URL+'/results/'+result_id+'/progress'
		r= requests.get(pollforjobstatus, verify=False,
									headers={'Authorization':solution.exercise.ac_token})
		time.sleep(6)
		if r.status_code == 200:
			if (r.json()['data']['status']=='Complete' or r.json()['data']['status']=='Error'):
				ac_result_response = r.json()['data']['status']
				print (r.json()['message'])
				break
			else:
				print (r.json()['message'])
		else:
			raise SystemExit("Failed to request progress\nError code: " + str(r.status_code))
	
	# get results
	get_result = BASE_URL+'/results/'+result_id+'/report'
	data = {"shared_key":solution.exercise.ac_token}
	r=requests.get(get_result, data=data, verify=False,
									headers={'Authorization':solution.exercise.ac_token})
	if r.status_code == 200:
		Get_results_from_solution(solution)
	else:
		raise SystemExit("Failed to request report\nError code: " + str(r.status_code))


def Get_sourcecodes_from_exercise(exercise):
	"""
		When given an exercise, return all source codes submitted as solutions.
		Source codes will be in separate files with username_exercise as file name.
	"""	
	
	# get result ids using solution ids
	for s in exercise.solutions:
		Get_sourcecodes_from_solution(s)
	
def Get_sourcecodes_from_solution(solution):
	"""
		When given a solution, return its. Source codes will be
		in separate files with username_exercise as file name.
		Returns path to file.
	"""	
	
	ac_token = solution.exercise.ac_token
	get_code = BASE_URL + '/solutions/' + solution.id
	data = {"shared_key" : ac_token}
	r=requests.get(get_code, data=data, verify=False, headers={
														'Authorization':ac_token})
	
	if r.status_code == 200:
		# create a list of result ids rather than a list of lists of result ids
		temp_code = r.json()['data']['code']
		
		if solution.user :
			file_name = solution.user.name + "_" + solution.exercise.name
		else :
			file_name = solution.id + solution.exercise.name
		code_file = open(file_name+".cpp", 'w+')
		code_file.write(temp_code)
		code_file.close()
		
		return file_name+".cpp"
		
	else:
		print "Error getting source code"
		return None



# --------------------------------- Main Functions -------------------------------

def Populate_exercise_list(username, password):
	# get a list of all exercises user has access to
	get_exercises = BASE_URL+'/exercise'
	r=requests.get(get_exercises, verify = False, auth=(username,password))
	if r.status_code == 200:
		exercise_list = r.json()['data']['exercises']
	else:
	    raise SystemExit("Authentication failed. Status code: " + str(r.status_code))
	
	# make a list of exercise names
	for e in exercise_list:
		temp_exercise = Exercise(e['name'])
		temp_exercise.id = e['exercise_id']
		temp_exercise.ac_token = e['access_token']
		exercises.append(temp_exercise)



			
			
			
			
			
			
			
			
			
			
			
			
			
				
			
					
		
